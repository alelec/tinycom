# based on:
# https://stackoverflow.com/a/26524823
# https://github.com/tartley/colorama
# http://en.m.wikipedia.org/wiki/ANSI_
# escape_code http://misc.flogisoft.com/bash/tip_
# colors_and_formatting http://invisible-island.net/xterm/ctlseqs/ctlseqs.html

import re
from Qt.QtCore import Qt
from Qt.QtGui import QTextCharFormat, QFont, QFontDatabase, QColor, QTextCursor


ANSI_CSI_RE = re.compile('\001?\033\\[((?:\\d|;)*)([a-zA-Z])\002?')  # Control Sequence Introducer
ANSI_OSC_RE = re.compile('\001?\033\\]((?:.|;)*?)(\x07)\002?')  # Operating System Command


def getColour(colorIndex, dark=False):
    if not dark:
        # switch (colorIndex) {
        if colorIndex == 0:
            color = Qt.darkGray

        elif colorIndex == 1:
            color = Qt.red

        elif colorIndex == 2:
            color = Qt.green

        elif colorIndex == 3:
            color = Qt.yellow

        elif colorIndex == 4:
            color = Qt.blue

        elif colorIndex == 5:
            color = Qt.magenta

        elif colorIndex == 6:
            color = Qt.cyan

        elif colorIndex == 7:
            color = Qt.white

        else:
            raise ValueError()

    else:
        # switch (colorIndex) {
        if colorIndex == 0:
            color = Qt.black

        elif colorIndex == 1:
            color = Qt.darkRed

        elif colorIndex == 2:
            color = Qt.darkGreen

        elif colorIndex == 3:
            color = Qt.darkYellow

        elif colorIndex == 4:
            color = Qt.darkBlue

        elif colorIndex == 5:
            color = Qt.darkMagenta

        elif colorIndex == 6:
            color = Qt.darkCyan

        elif colorIndex == 7:
            color = Qt.lightGray

        else:
            raise ValueError()

    return QColor(color)


def parseEscapeSequence(attribute: int, i: iter, textCharFormat: QTextCharFormat=None, default_text_format: QTextCharFormat=None):

    if textCharFormat is None:
        textCharFormat = QTextCharFormat()
    else:
        textCharFormat = QTextCharFormat(textCharFormat)

    # switch (attribute) {
    if attribute == 0:  # Normal/Default (reset all attributes)
        textCharFormat = default_text_format

    elif attribute == 1:  # Bold/Bright (bold or increased intensity)
        textCharFormat.setFontWeight(QFont.Bold)

    elif attribute == 2:  # Dim/Faint (decreased intensity)
        textCharFormat.setFontWeight(QFont.Light)

    elif attribute == 3:  # Italicized (italic on)
        textCharFormat.setFontItalic(True)

    elif attribute == 4:  # Underscore (single underlined)
        textCharFormat.setUnderlineStyle(QTextCharFormat.SingleUnderline)
        textCharFormat.setFontUnderline(True)

    elif attribute == 5:  # Blink (slow, appears as Bold)
        textCharFormat.setFontWeight(QFont.Bold)

    elif attribute == 6:  # Blink (rapid, appears as very Bold)
        textCharFormat.setFontWeight(QFont.Black)

    elif attribute == 7:  # Reverse/Inverse (swap foreground and background)
        foregroundBrush = textCharFormat.foreground()
        textCharFormat.setForeground(textCharFormat.background())
        textCharFormat.setBackground(foregroundBrush)

    elif attribute == 8:  # Concealed/Hidden/Invisible (usefull for passwords)
        textCharFormat.setForeground(textCharFormat.background())

    elif attribute == 9:  # Crossed-out characters
        textCharFormat.setFontStrikeOut(True)

    elif attribute == 10:  # Primary (default) font
        textCharFormat.setFont(default_text_format.font())

    elif 11 <= attribute <= 19 :
        fontDatabase = QFontDatabase()
        fontFamily = textCharFormat.fontFamily()
        fontStyles = fontDatabase.styles(fontFamily)
        fontStyleIndex = attribute - 11
        if (fontStyleIndex < fontStyles.length()):
            textCharFormat.setFont(fontDatabase.font(fontFamily, fontStyles.at(fontStyleIndex), textCharFormat.font().pointSize()))

    elif attribute == 20:  # Fraktur (unsupported)
        pass

    elif attribute == 21:  # Set Bold off
        textCharFormat.setFontWeight(QFont.Normal)

    elif attribute == 22:  # Set Dim off
        textCharFormat.setFontWeight(QFont.Normal)

    elif attribute == 23:  # Unset italic and unset fraktur
        textCharFormat.setFontItalic(False)

    elif attribute == 24:  # Unset underlining
        textCharFormat.setUnderlineStyle(QTextCharFormat.NoUnderline)
        textCharFormat.setFontUnderline(False)

    elif attribute == 25:  # Unset Blink/Bold
        textCharFormat.setFontWeight(QFont.Normal)

    elif attribute == 26:  # Reserved
        pass

    elif attribute == 27:  # Positive (non-inverted)
        backgroundBrush = textCharFormat.background()
        textCharFormat.setBackground(textCharFormat.foreground())
        textCharFormat.setForeground(backgroundBrush)

    elif attribute == 28:
        textCharFormat.setForeground(default_text_format.foreground())
        textCharFormat.setBackground(default_text_format.background())

    elif attribute == 29:
        textCharFormat.setUnderlineStyle(QTextCharFormat.NoUnderline)
        textCharFormat.setFontUnderline(False)

    elif 30 <= attribute <= 37:
        colorIndex = attribute - 30
        color = getColour(colorIndex, QFont.Normal < textCharFormat.fontWeight())
        textCharFormat.setForeground(color)

    elif attribute == 38:
        try:
            ok = False
            selector = int(i.next())
            color = QColor()
            # switch (selector) {
            if selector == 2:
                red = int(i.next())
                green = int(i.next())
                blue = int(i.next())
                color.setRgb(red, green, blue)

            elif selector == 5:

                index = int(i.next())
                # switch (index) {
                if 0x00 <= index <= 0x07: # 0x00-0x07:  standard colors (as in ESC [ 30..37 m)
                    return parseEscapeSequence(index - 0x00 + 30, i, textCharFormat, default_text_format)
                
                if 0x08 <= index <= 0x0F: #  0x08-0x0F:  high intensity colors (as in ESC [ 90..97 m)
                    return parseEscapeSequence(index - 0x08 + 90, i, textCharFormat, default_text_format)
                
                if 0x10 <= index <= 0xE7: #  0x10-0xE7:  6*6*6=216 colors: 16 + 36*r + 6*g + b (0≤r,g,b≤5)
                    index -= 0x10
                    red = index % 6
                    index /= 6
                    green = index % 6
                    index /= 6
                    blue = index % 6
                    index /= 6
                    # Q_ASSERT(index == 0)
                    color.setRgb(red, green, blue)

                if 0xE8 <= index <= 0xFF:  #  0xE8-0xFF:  grayscale from black to white in 24 steps
                    intensity = float(index - 0xE8) / (0xFF - 0xE8)
                    color.setRgbF(intensity, intensity, intensity)

                textCharFormat.setForeground(color)

        except StopIteration:
            raise ValueError("Insufficient details for colour selection")

    elif attribute == 39:
        textCharFormat.setForeground(default_text_format.foreground())

    elif 40 <= attribute <= 47:
        colorIndex = attribute - 40
        color = getColour(colorIndex)
        textCharFormat.setBackground(color)

    elif attribute == 48:
        # if (i.hasNext()) {
            ok = False
            selector = int(i.next())
            # Q_ASSERT(ok)
            color = QColor()
            # switch (selector) {
            if selector == 2:
               # if (!i.hasNext()) {
               red = int(i.next())
               # Q_ASSERT(ok)
               # if (!i.hasNext()) {
               green = int(i.next())
               # Q_ASSERT(ok)
               # if (!i.hasNext()) {
               blue = int(i.next())
               # Q_ASSERT(ok)
               color.setRgb(red, green, blue)

            elif selector == 5:
                # if (!i.hasNext()) {
                index = int(i.next())
                # Q_ASSERT(ok)
                # switch (index) {
                if 0x00 <= index <= 0x07:  #  0x00-0x07:  standard colors (as in ESC [ 40..47 m)
                    return parseEscapeSequence(index - 0x00 + 40, i, textCharFormat, default_text_format)
                
                if 0x08 <= index <= 0x0F:  #  0x08-0x0F:  high intensity colors (as in ESC [ 100..107 m)
                    return parseEscapeSequence(index - 0x08 + 100, i, textCharFormat, default_text_format)
                
                if 0x10 <= index <= 0xE7:  #  0x10-0xE7:  6*6*6=216 colors: 16 + 36*r + 6*g + b (0≤r,g,b≤5)
                    index -= 0x10
                    red = index % 6
                    index /= 6
                    green = index % 6
                    index /= 6
                    blue = index % 6
                    index /= 6
                    # Q_ASSERT(index == 0)
                    color.setRgb(red, green, blue)

                if 0xE8 <= index <= 0xFF:  #  0xE8-0xFF:  grayscale from black to white in 24 steps
                    intensity = float(index - 0xE8) / (0xFF - 0xE8)
                    color.setRgbF(intensity, intensity, intensity)

                textCharFormat.setBackground(color)

    elif attribute == 49:
        textCharFormat.setBackground(default_text_format.background())

    elif 90 <= attribute <= 97:
        colorIndex = attribute - 90
        color = getColour(colorIndex)
        color.setRedF(color.redF() * 0.8)
        color.setGreenF(color.greenF() * 0.8)
        color.setBlueF(color.blueF() * 0.8)
        textCharFormat.setForeground(color)

    if 100 <= attribute <= 107:
        colorIndex = attribute - 100
        color = getColour(colorIndex)
        color.setRedF(color.redF() * 0.8)
        color.setGreenF(color.greenF() * 0.8)
        color.setBlueF(color.blueF() * 0.8)
        textCharFormat.setBackground(color)

    return textCharFormat


def set_cursor_position(cursor: QTextCursor, position=None, on_stderr=False):
    """
    :param QTextCursor cursor:
    :param Tuple[X,Y] position:
    :param on_stderr:
    :return:
    """
    if position is None:
        return
    cursor.movePosition(QTextCursor.Start)
    cursor_adjust(cursor, *position)


def cursor_adjust(cursor: QTextCursor, x, y, on_stderr=False, select=False):
    anchor = QTextCursor.KeepAnchor if select else QTextCursor.MoveAnchor
    if y:
        if y > 0:
            cursor.movePosition(QTextCursor.Down, anchor, y)
        else:
            cursor.movePosition(QTextCursor.Up, anchor, abs(y))
    if x:
        if x > 0:
            cursor.movePosition(QTextCursor.Right, anchor, x)
        else:
            cursor.movePosition(QTextCursor.Left, anchor, abs(x))
    # if not select:
    #     cursor.clearSelection()


def erase_screen(cursor: QTextCursor, mode=0, on_stderr=False):
    # 0 should clear from the cursor to the end of the screen.
    # 1 should clear from the cursor to the beginning of the screen.
    # 2 should clear the entire screen, and move cursor to (1,1)
    if mode == 0:
        cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
    elif mode == 1:
        cursor.movePosition(QTextCursor.Start, QTextCursor.KeepAnchor)
    elif mode == 2:
        cursor.movePosition(QTextCursor.Start, QTextCursor.MoveAnchor)
        cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
    else:
        # invalid mode
        return
    cursor.deleteChar()


def erase_line(cursor: QTextCursor, mode=0, on_stderr=False):
    # 0 should clear from the cursor to the end of the line.
    # 1 should clear from the cursor to the beginning of the line.
    # 2 should clear the entire line.
    if mode == 0:
        cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
    elif mode == 1:
        cursor.movePosition(QTextCursor.StartOfLine, QTextCursor.KeepAnchor)
    elif mode == 2:
        cursor.movePosition(QTextCursor.StartOfLine, QTextCursor.MoveAnchor)
        cursor.movePosition(QTextCursor.EndOfLine, QTextCursor.KeepAnchor)
    else:
        # invalid mode
        return
    cursor.deleteChar()


def extract_params(command, paramstring):
    if command in 'Hf':
        params = tuple(int(p) if len(p) != 0 else 1 for p in paramstring.split(';'))
        while len(params) < 2:
            # defaults:
            params = params + (1,)
    else:
        params = tuple(int(p) for p in paramstring.split(';') if len(p) != 0)
        if len(params) == 0:
            # defaults:
            if command in 'JKm':
                params = (0,)
            elif command in 'ABCD':
                params = (1,)

    return params


def _overwrite(span, cursor: QTextCursor, textCharFormat):
    cursor_adjust(cursor, x=len(span), y=0, select=True)
    span = span.replace('\x00', '')
    cursor.insertText(span, textCharFormat)


def _write(span, cursor: QTextCursor, textCharFormat: QTextCharFormat):
    # Handle backspace
    bpos = bend = 0
    for bs in re.finditer('\b', span):
        bstart, bend = bs.span()
        bspan = span[bpos:bstart]
        _overwrite(bspan, cursor, textCharFormat)
        cursor_adjust(cursor, -1, 0)
        bpos = bend
    _overwrite(span[bend:], cursor, textCharFormat)


def write(text, cursor: QTextCursor, textCharFormat, default_text_format: QTextCharFormat):
    pos = end = 0
    for match in re.finditer(ANSI_CSI_RE, text):
        start, end = match.span()
        span = text[pos:start]

        _write(span, cursor, textCharFormat)

        paramstring, command = match.groups()
        params = extract_params(command, paramstring)
        if command == 'm':
            textCharFormat = parseEscapeSequence(params[0], iter(params[1:]),
                                                 textCharFormat=textCharFormat, default_text_format=default_text_format)
        elif command in 'J':
            erase_screen(cursor, params[0])
        elif command in 'K':
            erase_line(cursor, params[0])
        elif command in 'Hf':  # cursor position - absolute
            set_cursor_position(cursor, params)
        elif command in 'ABCD':  # cursor position - relative
            n = params[0]
            # A - up, B - down, C - forward, D - back
            x, y = {'A': (0, -n), 'B': (0, n), 'C': (n, 0), 'D': (-n, 0)}[command]
            cursor_adjust(cursor, x, y)

        else:
            print("Don't know what to do with escape %s" % command)
        pos = end

    # Write the remainder
    _write(text[end:], cursor, textCharFormat)
    return textCharFormat
