@echo off

pushd %~dp0


if exist "%VIRTUAL_ENV%"\Lib\site-packages\PySide\pyside-rcc.exe (
    echo Updating PySide Resources...
    "%VIRTUAL_ENV%"\Lib\site-packages\PySide\pyside-rcc.exe -py3 -o ..\tinycom_rc.py tinycom.qrc
)

if exist "%VIRTUAL_ENV%"\Lib\site-packages\PySide2\pyside2-rcc.exe (
    echo Updating PySide2 Resources...
    "%VIRTUAL_ENV%"\Lib\site-packages\PySide2\pyside2-rcc.exe -py3 -o ..\tinycom_rc.py tinycom.qrc
)

popd
