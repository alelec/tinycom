# Copyright (c) 2017 Joshua Henderson <digitalpeer@digitalpeer.com>
#
# SPDX-License-Identifier: GPL-3.0
"""
Wraps a serial port in a thread.
"""
import time

import threading
import serial
from Qt import QtCore
from contextlib import contextmanager


@contextmanager
def acquire_timeout(lock, timeout):
    result = lock.acquire(timeout=timeout)
    yield result
    if result:
        lock.release()


class SerialThread(QtCore.QThread):
    """Serial thread."""

    recv = QtCore.Signal(bytes, name='recv')
    recv_error = QtCore.Signal(str, name='recv_error')

    def __init__(self, serial_instance):
        super(SerialThread, self).__init__()

        self.serial = serial_instance
        self.alive = True
        self._lock = threading.Lock()

    #def __del__(self):
    #    self.stop()

    def stop(self):
        """Stop the thread from running."""
        self.alive = False
        if hasattr(self.serial, 'cancel_read'):
            try:
                self.serial.cancel_read()
                time.sleep(0.1)
            except TypeError:
                pass
        print("wait for serial the end")
        self.serial.close()
        time.sleep(0.1)
        self.wait()

    def run(self):
        """Thread run loop."""
        self.alive = True
        error = None
        while self.alive:
            if not self.serial.isOpen():
                error = "closed"
                break
            else:
                try:
                    data = self.serial.read(1024 * 8)
                    # if not data:
                    #     # On some platforms the port does not close when device unplugged, reconnect to test for this.
                    #     self.serial.close()
                    #     self.serial.open()

                except serial.SerialException as exp:
                    error = str(exp)
                    break
                else:
                    if data:
                        try:
                            self.recv.emit(data)
                        except Exception as exp:  # pylint: disable=broad-except
                            error = str(exp)
                            break
        if error is not None:
            self.recv_error.emit(error)
        print("Serial ended")

    def write(self, data):
        """Write to the port with lock held."""
        with acquire_timeout(self._lock, 2):
            return self.serial.write(data)

    def close(self):
        """Stop the thread and close the serial port with lock held."""
        with acquire_timeout(self._lock, 2):
            print("before stop")
            self.stop()
            print("before close")
            self.serial.close()
