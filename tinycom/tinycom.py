#!/usr/bin/python
#
# A simple line based GUI serial terminal.
#
# Copyright (c) 2017 Joshua Henderson <digitalpeer@digitalpeer.com>
#
# SPDX-License-Identifier: GPL-3.0

"""TinyCom"""
import os
import sys
import codecs
import serial
from copy import copy
from datetime import datetime
from functools import partial
from serial.tools import list_ports
from pkg_resources import parse_version
from Qt.QtCore import Qt
from Qt import QtWidgets, QtCore, QtGui, _loadUi
try:
    import PySide2
    from .pyside_dynamic import loadUi
except ImportError:
    loadUi = _loadUi

from .version import __version__
from . import guisave, ascii_escape
from .lineedit import CustomLineEdit


# By default, a thread is used to process the serial port. If this is set to
# False, a timer will poll the serial port at a fixed interval, which can have
# obvious negative side effects of delayed recv.
USE_THREAD = True

if USE_THREAD:
    from . import serialthread  # pylint: disable=wrong-import-position


def populate_serial_ports():
    """Gather all serial ports found on system."""
    ports = list(list_ports.comports())
    return {"%s: %s" % (p[0], p[1]): p for p in ports}


def _chunks(text, chunk_size):
    """Chunk text into chunk_size."""
    for i in range(0, len(text), chunk_size):
        yield text[i:i+chunk_size]

def str_to_hex(text):
    """Convert text to hex encoded bytes."""
    return ''.join('{:02x}'.format(ord(c)) for c in text)

def hex_to_raw(hexstr):
    """Convert a hex encoded string to raw bytes."""
    return ''.join(chr(int(x, 16)) for x in _chunks(hexstr, 2))

def human_size(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


class MainWindow(QtWidgets.QMainWindow):
    """The main window."""
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        loadUi(os.path.join(os.path.dirname(__file__), 'tinycom.ui'), self,
               dict(CustomLineEdit=CustomLineEdit))

        font = self.log.currentFont()
        font.setFamily("Consolas")
        font.setStyleHint(QtGui.QFont.Monospace)
        self.log.setCurrentFont(font)

        self.serial = None
        self.rx = 0
        self.tx = 0
        self.history_index = 0
        self._log_file_handle = None
        self._log_scroll = None
        self._writing_to_log = True

        self.statusBar().showMessage("Not connected")

        try:
            self.ports = populate_serial_ports()
        except EnvironmentError:
            pass

        if not self.ports:
            self.statusBar().showMessage(
                'No serial ports found.  You can try manually entering one.')

        self.btn_open.clicked.connect(self.onBtnOpen)
        self.btn_send.clicked.connect(self.onBtnSend)
        self.input.returnPressed.connect(self.onBtnSend)
        self.input.textChanged.connect(self.onInputChanged)
        self.line_end.currentIndexChanged.connect(self.onInputChanged)
        self.btn_clear.clicked.connect(self.onBtnClear)
        self.btn_open_log.clicked.connect(self.onBtnOpenLog)
        self.actionQuit.triggered.connect(self.close)
        self.actionAbout.triggered.connect(self.onAbout)
        self.history.itemDoubleClicked.connect(self.onHistoryDoubleClick)
        self.history.itemSelectionChanged.connect(self.onHistoryItemSelectionChanged)
        self.log.verticalScrollBar().valueChanged.connect(self.onScrollChanged)
        self.baudrate.currentIndexChanged.connect(self.onBaudChanged)
        self.check_wrap.stateChanged.connect(self.onWrapChanged)
        self.onWrapChanged()

        self.enable_log.clicked.connect(self.logfile_close)

        self.port.installEventFilter(self)
        self.port.currentIndexChanged.connect(self.on_port_changed)

        self.input.setEnabled(False)
        self.btn_send.setEnabled(False)

        self.rxtx = QtWidgets.QLabel("TX: 0 B  RX: 0 B")
        self.statusBar().addPermanentWidget(self.rxtx)

        self.settings_frame_show.toggled.connect(partial(self.onToggle, self.settings_frame))
        self.settings_frame_show.click()

        self.settings = QtCore.QSettings('tinycom', 'tinycom')
        self.settings.beginGroup("mainWindow")
        guisave.load(self, self.settings)
        self.settings.endGroup()

        if parse_version(serial.VERSION) >= parse_version("3.0"):
            self.serial = serial.Serial(timeout=0.25,
                                        write_timeout=5.0,
                                        inter_byte_timeout=1.0)
        else:
            self.serial = serial.Serial(timeout=0.25,
                                        writeTimeout=5.0,
                                        interCharTimeout=1.0)
        if not USE_THREAD:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.doReadData)
        else:
            self.thread = serialthread.SerialThread(self.serial)
            self.thread.recv.connect(self.recv)
            self.thread.recv_error.connect(self.onRecvError)

        self.retry_timer = QtCore.QTimer()
        self.retry_timer.timeout.connect(partial(self.onBtnOpen, True))

        # Type directly into terminal
        self.input.key_event.connect(self.onInputKey)
        self.log.installEventFilter(self)
        self.log.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.cursor = self.log.textCursor()
        self._default_text_format = QtGui.QTextCharFormat(self.cursor.charFormat())
        self.current_text_format = copy(self.cursor.charFormat())
        self.log_position = self.cursor.position()

        # Settings
        self.refreshPorts()

        self.baudrate.setCurrentIndex(self.baudrate.findText("115200"))
        self.baudrate.setEditable(True)

        self.bytesize.addItem("5", serial.FIVEBITS)
        self.bytesize.addItem("6", serial.SIXBITS)
        self.bytesize.addItem("7", serial.SEVENBITS)
        self.bytesize.addItem("8", serial.EIGHTBITS)
        self.bytesize.setCurrentIndex(self.bytesize.findText("8"))

        self.parity.addItem("None", serial.PARITY_NONE)
        self.parity.addItem("Even", serial.PARITY_EVEN)
        self.parity.addItem("Odd", serial.PARITY_ODD)
        self.parity.addItem("Mark", serial.PARITY_MARK)
        self.parity.addItem("Space", serial.PARITY_SPACE)
        self.parity.setCurrentIndex(self.parity.findText("None"))

        self.stopbits.addItem("1", serial.STOPBITS_ONE)
        self.stopbits.addItem("1.5", serial.STOPBITS_ONE_POINT_FIVE)
        self.stopbits.addItem("2", serial.STOPBITS_TWO)
        self.stopbits.setCurrentIndex(self.stopbits.findText("1"))

        self.settings = QtCore.QSettings('tinycom', 'tinycom')
        self.settings.beginGroup("settingsDialog")
        guisave.load(self, self.settings)
        self.settings.endGroup()

        self.history.installEventFilter(self)

        self.last = None

    @property
    def default_text_format(self):
        return QtGui.QTextCharFormat(self._default_text_format)

    def refreshPorts(self):
        try:
            self.ports = populate_serial_ports()
            self.port.clear()
            self.port.addItems(list(self.ports.keys()))
        except EnvironmentError:
            pass

    def getSettings(self):
        """
        Return a dictionary of settings.
        This returns direct attributes of the serial object.
        """
        return {'port': self.port.currentText().split(':')[0],
                'baudrate': int(self.baudrate.currentText()),
                'bytesize': self.bytesize.itemData(self.bytesize.currentIndex()),
                'parity': self.parity.itemData(self.parity.currentIndex()),
                'stopbits': self.stopbits.itemData(self.stopbits.currentIndex()),
                'xonxoff': self.xonxoff.isChecked(),
                'rtscts': self.rtscts.isChecked(),
                'dsrdtr': self.dsrdtr.isChecked()}

    def saveSettings(self):
        """Accept changes."""
        self.settings.beginGroup("settingsDialog")
        guisave.save(self, self.settings,
                     ["port", "baudrate", "bytesize", "parity", "stopbits",
                      "xonxoff", "rtscts", "dsrdtr", "history"])
        self.settings.endGroup()

    def on_port_changed(self, value):
        if self.serial.isOpen():
            self.onBtnOpen()

    def eventFilter(self, source, event):
        if source is self.port and event.type() == QtCore.QEvent.MouseButtonPress:
            self.refreshPorts()

        if source is self.history and event.type() == QtCore.QEvent.KeyPress:
            if event.key() == Qt.Key_Delete or event.key() == Qt.Key_Backspace:
                selected = list(self.history.selectedItems())
                for item in selected:
                    row_index = self.history.row(item)
                    self.history.takeItem(row_index)
                    self.history_index = min(self.history.count(), row_index+1)
                    self.history.setCurrentRow(self.history_index-1)
                    self.history.scrollToItem(self.history.item(min(self.history.count()-1, row_index+1)))

                return True

        if source is self.log and event.type() == QtCore.QEvent.MouseButtonPress:
            if event.button() == QtCore.Qt.MouseButton.RightButton:
                cursor = self.log.textCursor()
                if cursor.selectedText():
                    self.log.copy()
                else:
                    clipboard = QtGui.QClipboard().text()
                    if clipboard and self.serial.is_open:
                        self.send_text(clipboard)
                cursor.clearSelection()
                self.log.setTextCursor(cursor)
                return True

        if source is self.log and event.type() == QtCore.QEvent.KeyPress:
            data = event.text().encode()
            if not data:
                keys_codes = {
                    Qt.Key_Home: b'\x01',  # ^A
                    Qt.Key_End: b'\x05',  # ^E
                    Qt.Key_Left: b'\x1B[D',
                    Qt.Key_Up: b'\x1B[A',
                    Qt.Key_Right: b'\x1B[C',
                    Qt.Key_Down: b'\x1B[B',
                }
                data = keys_codes.get(event.key(), None)
            if data and self.serial.is_open:
                self.send_text(data)
            return True

        return super(MainWindow, self).eventFilter(source, event)

    def uiConnectedEnable(self, connected):
        """Toggle enabled on controls based on connect."""
        if connected:
            self.btn_open.setText("&Close Device")
            self.onInputChanged()
        else:
            self.btn_open.setText("&Open Device")
            self.btn_send.setEnabled(connected)
        self.input.setEnabled(connected)

    def logfile_close(self):
        if self._log_file_handle is not None:
            self._log_file_handle.flush()
            self._log_file_handle.close()
            self._log_file_handle = None

    def onBtnOpen(self, quiet=False, just_close=False):
        """Open button clicked."""
        if self.serial.isOpen() or just_close:
            if not USE_THREAD:
                self.timer.stop()
                self.serial.close()
            else:
                print("about to close thread")
                self.thread.close()
            print("closed serial")
            self.uiConnectedEnable(False)
            self.statusBar().showMessage("Not connected")
            self.btn_open.setChecked(False)
            self.btn_open.setText("Open Device")
            self.logfile_close()

        else:
            settings = self.getSettings()
            for key in settings:
                setattr(self.serial, key, settings[key])

            try:
                self.serial.open()
            except serial.SerialException as exp:
                if not quiet:
                    QtWidgets.QMessageBox.critical(self, 'Error Opening Serial Port',
                                               str(exp))
            except (IOError, OSError) as exp:
                if not quiet:
                    QtWidgets.QMessageBox.critical(self, 'IO Error Opening Serial Port',
                                               str(exp))
            else:
                self.retry_timer.stop()
                if parse_version(serial.VERSION) >= parse_version("3.0"):
                    self.serial.reset_input_buffer()  # pylint: disable=no-member
                    self.serial.reset_output_buffer()  # pylint: disable=no-member
                else:
                    self.serial.flushInput()  # pylint: disable=no-member
                    self.serial.flushOutput()  # pylint: disable=no-member
                self.statusBar().showMessage('Connected to ' + settings['port'] +
                                             ' ' +
                                             str(settings['baudrate']) + ',' +
                                             str(settings['parity']) + ',' +
                                             str(settings['bytesize']) + ',' +
                                             str(settings['stopbits']))
                self.uiConnectedEnable(True)
                if not USE_THREAD:
                    self.timer.start(100)
                else:
                    self.thread.start()

                self.saveSettings()
                self.btn_open.setChecked(True)
                self.btn_open.setText("Close Device")

    def doLog(self, text):
        """Write to log file."""
        text = text.decode("utf-8", 'backslashreplace')
        log = ""

        self._writing_to_log = True
        self.cursor.setPosition(self.log_position)

        if self.output_hex.isChecked():
            text = str_to_hex(text)
            text = ' '.join(a+b for a, b in zip(text[::2], text[1::2]))
            text = text + ' '
            self.cursor.movePosition(QtGui.QTextCursor.End)
            self.cursor.insertText(text)
            log = text
        else:
            if self.add_timestamps.isChecked():
                ts = datetime.now().strftime("%Y%m%d %H:%M:%S : ")
                if self.last == '\n':
                    # End of previous text received was newline
                    self.cursor.insertText(ts, self.default_text_format)
                    log += ts

                for i, t in enumerate(text.rstrip('\n').split('\n')):
                    if i:
                        self.cursor.insertText(ts, self.default_text_format)
                        log += ts
                    if t:
                        self.current_text_format = ascii_escape.write(t, self.cursor,
                                                                      self.current_text_format, self.default_text_format)
                        log += t
            else:
                self.current_text_format = ascii_escape.write(text, self.cursor,
                                                              self.current_text_format, self.default_text_format)
                log = text
            self.last = text[-1]

        self.log_position = self.cursor.position()
        self.log.setTextCursor(self.cursor)

        if self.enable_log.isChecked():
            try:
                filename = self.log_file.text()
                if len(filename) and log:
                    if self._log_file_handle and self._log_file_handle.name != filename:
                        self.logfile_close()
                    if self._log_file_handle is None:
                        self._log_file_handle = open(self.log_file.text(), "ab")
                self._log_file_handle.write(log.encode('utf-8', errors='ignore'))
            except OSError:
                self.enable_log.setChecked(False)
        else:
            self.logfile_close()

        if self._log_scroll is not None:
            self.log.verticalScrollBar().setValue(self._log_scroll)
        self._writing_to_log = False

    def send_text(self, text, add_newline=False):
        if not self.serial.isOpen():
            return

        if isinstance(text, str):
            raw = self.convert_endings(text, add_on_end=add_newline).encode()
        else:
            raw = text

        ret = 0
        if raw:
            try:
                ret = self.serial.write(raw)
            except serial.SerialException as exp:
                QtWidgets.QMessageBox.critical(self, 'Serial write error', str(exp))

        return ret

    def convert_endings(self, text, add_on_end=True):
        endings = [u"\n", u"\r", u"\r\n", u"\n\r", u"", u""]
        current = endings[self.line_end.currentIndex()]
        others = [e for e in endings if e and e != current]

        if len(current) == 0:
            for e in others:
                if e in text:
                    text = text.replace(e, current)
                    break

        if len(current) == 1:
            for e in sorted(others, reverse=True, key=lambda e: len(e)):
                if e in text:
                    text = text.replace(e, current)
                    break

        if len(current) == 2:
            for e in sorted(others, reverse=False, key=lambda e: len(e)):
                if e in text:
                    text = text.replace(e, current)
                    break

        if add_on_end:
            text += current
        return text

    def encodeInput(self):
        """
        Interpret the user input text as hex or append appropriate line ending.
        """
        text = self.input.text()
        if not text:
            return

        if self.line_end.currentText() == "Hex":
            text = ''.join(text.split())
            if len(text) % 2:
                raise ValueError('Hex encoded values must be a multiple of 2')
            text = hex_to_raw(text)
        else:
            text = self.convert_endings(text)
        return text

    def onInputChanged(self):
        """Input line edit changed."""
        try:
            self.encodeInput()
        except ValueError:
            self.input.setStyleSheet("color: rgb(255, 0, 0);")
            self.btn_send.setEnabled(False)
            return
        self.input.setStyleSheet("color: rgb(0, 0, 0);")
        if self.serial is not None and self.serial.isOpen():
            self.btn_send.setEnabled(True)

    def onInputKey(self, key):
        """Input line edit key pressed."""
        if key == QtCore.Qt.Key_Up:
            if self.history_index > 0:
                self.history_index -= 1
                item = self.history.item(self.history_index)
                self.input.setText(item.text())
        elif key == QtCore.Qt.Key_Down:
            if self.history_index < self.history.count():
                self.history_index += 1
                if self.history_index == self.history.count():
                    self.input.setText("")
                else:
                    item = self.history.item(self.history_index)
                    self.input.setText(item.text())

    def onBtnSend(self):
        """Send button clicked."""
        if not self.serial.isOpen():
            return
        try:
            raw = self.encodeInput()
            ret = self.send_text(raw, add_newline=True)
            self.tx = self.tx + ret
            self.rxtx.setText("TX: " + human_size(self.tx) + "  RX: " +
                              human_size(self.rx))
        except serial.SerialException as exp:
            QtWidgets.QMessageBox.critical(self, 'Serial write error', str(exp))
            return
        except ValueError as exp:
            QtWidgets.QMessageBox.critical(self, 'Input Error', str(exp))
            return

        if self.echo_input.isChecked():
            self.doLog(raw)

        input_text = self.input.text()
        self.input.clear()

        if input_text:
            prev_item = self.history.item(self.history.count()-1)
            if prev_item and prev_item.text() == input_text:
                return

        item = QtWidgets.QListWidgetItem(input_text)
        self.history.addItem(item)
        self.history.scrollToItem(item)
        self.history_index = self.history.count()

    def onBtnOpenLog(self):
        """Open log file button clicked."""
        dialog = QtWidgets.QFileDialog(self)
        dialog.setWindowTitle('Open File')
        dialog.setNameFilter("All files (*.*)")
        dialog.setFileMode(QtWidgets.QFileDialog.AnyFile)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            filename = dialog.selectedFiles()[0]
            self.log_file.setText(filename)

    def onHistoryDoubleClick(self, item):
        """Send log item double clicked."""
        self.input.setText(item.text())
        self.onBtnSend()

    def onHistoryItemSelectionChanged(self):
        """Send log item double clicked."""
        currentText = '\n'.join([item.text() for item in self.history.selectedItems()])
        self.input.setText(currentText)

    def onScrollChanged(self):
        if self._writing_to_log:
            return

        current_scroll = self.log.verticalScrollBar().value()
        if current_scroll == self.log.verticalScrollBar().maximum():
            self._log_scroll = None
        else:
            self._log_scroll = current_scroll

    def onWrapChanged(self):
        self.log.setLineWrapMode(
            QtWidgets.QTextEdit.WidgetWidth if self.check_wrap.isChecked() else QtWidgets.QTextEdit.NoWrap
        )

    def onBaudChanged(self):
        if self.serial:
            try:
                self.serial.baudrate = int(self.baudrate.currentText())
            except ValueError:
                self.baudrate.setCurrentIndex(self.baudrate.findText(str(self.serial.baudrate)))

    def onBtnClear(self):
        """Clear button clicked."""
        self.log.clear()

    def doReadData(self):
        """Read serial port."""
        if self.serial.isOpen:
            try:
                text = self.serial.read(2048)
            except serial.SerialException as exp:
                QtWidgets.QMessageBox.critical(self, 'Serial read error', str(exp))
            else:
                self.recv(text)

    def recv(self, text):
        """Receive data from the serial port signal."""
        if len(text):
            size = len(text)
            self.rx = self.rx + size
            self.rxtx.setText("TX: " + human_size(self.tx) + "  RX: " +
                              human_size(self.rx))
            self.doLog(text)

    def onRecvError(self, error):
        """Receive error when reading serial port from signal."""
        # QtWidgets.QMessageBox.critical(self, 'Serial read error', error)
        self.onBtnOpen(just_close=True)
        self.retry_timer.start(100)

    def onAbout(self):
        """About menu clicked."""
        msg = QtWidgets.QMessageBox(self)
        image = QtGui.QImage(":/icons/32x32/tinycom.png")
        pixmap = QtGui.QPixmap(image).scaledToHeight(32,
                                               QtCore.Qt.SmoothTransformation)
        msg.setIconPixmap(pixmap)
        msg.setInformativeText("Copyright (c) 2017 Joshua Henderson\nCopyright (c) 2022 Andrew Leech")
        msg.setWindowTitle("TinyCom " + __version__)
        with codecs.open(os.path.join(os.path.dirname(__file__), 'LICENSE.txt'),
                         encoding='utf-8') as f:
            msg.setDetailedText(f.read())
        msg.setText(
            "<p><b>TinyCom</b> is a simple line based serial terminal GUI"
            " written in Python. This is a tool that's useful for talking to a"
            " variety of serial based hardware that can involve custom protocols"
            " or just a standard command line interface.  It runs on anything"
            " that supports Python and Qt.")

        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def closeEvent(self, unused_event):
        """Handle window close event."""
        _ = unused_event
        if self._log_file_handle is not None:
            self._log_file_handle.close()
            self._log_file_handle = None
        if not USE_THREAD:
            self.timer.stop()
            self.serial.close()
        else:
            self.thread.close()

        self.saveSettings()
        self.settings.beginGroup("mainWindow")
        guisave.save(self, self.settings,
                     ["ui", "remove_escape", "add_timestamps",
                      "echo_input", "log_file", "enable_log", "line_end",
                      "splitter", "output_hex", "settings_frame_show", "check_wrap"])
        self.settings.endGroup()

    def onToggle(self, dest, checked):
        if checked:
            dest.show()
        else:
            dest.hide()


def main():
    """Create main app and window."""
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("TinyCom")
    win = MainWindow(None)
    win.setWindowTitle("TinyCom")
    win.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
